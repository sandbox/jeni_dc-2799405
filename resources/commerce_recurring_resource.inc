<?php

/**
 * @file
 * Commerce Recurring Services resources.
 */

/**
 * Callback to list commerce recurring entities.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $fields
 *   The fields you want returned.
 * @param $parameters
 *   An array containing fields and values used to build a sql WHERE clause
 *   indicating items to retrieve.
 * @param $page_size
 *   Integer number of items to be returned.
 * @param $options
 *   Additional query options.
 * @param $status
 *   Integer. Status of the recurring entity.
 * @return
 *   An array of node objects.
 */
function _commerce_recurring_resource_index($page, $fields, $parameters, $page_size, $options = array(), $status) {
  $recurring_entity_select = db_select('commerce_recurring', 't')
    ->addTag('commerce_recurring');
  services_resource_build_index_query($recurring_entity_select, $page, $fields, $parameters, $page_size, 'commerce_recurring', $options);
  if (!empty($status || $status === '0')) {
    $recurring_entity_select->condition('status', $status);
  }
  $results = services_resource_execute_index_query($recurring_entity_select);
  return services_resource_build_index_list($results, 'commerce_recurring', 'id');
}

/**
 * Callback to cancel a recurring entity.
 *
 * @param $id
 *    Integer ID of the entity to be cancelled.
 */
function _commerce_recurring_resource_cancel($id) {  
  $recurring_entity = commerce_recurring_load($id);
  if (is_object($recurring_entity)) {
    if ($recurring_entity->status == '0') {
      return t('Recurring entity @id is already disabled.', array('@id' => $id));
    }
    else {
      commerce_recurring_stop_recurring($recurring_entity);
      rules_invoke_event('commerce_recurring_subscription_cancelled', $recurring_entity);
      return t('Recurring entity @id has been cancelled.', array('@id' => $id));
    }
  }
  return services_error(t('Recurring entity @id could not be found.', array('@id' => $id)), 404);
}

/**
 * Access callback for the cancel action.
 */
function _commerce_recurring_resource_cancel_access($args = array()) {
  global $user;
  $recurring_entity = commerce_recurring_load($args['0']);
  if (is_object($recurring_entity) && user_access('cancel own subscription renewals') && $recurring_entity->uid == $user->uid) {
    return TRUE;
  }
  return services_error(t('Access denied.'), 403);
}
